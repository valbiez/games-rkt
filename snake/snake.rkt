#lang racket
(require 2htdp/image)
(require 2htdp/universe)

;;;;;;;;;;;;;;;;
; Données
;;;;;;;;;;;;;;;;

(define-struct monde(serpent bonus dir lost? stop nouveaux-segments menu-option? bord-collision?))

;bonus=liste de points
;serpent=liste des points
;dir=direction du serpent
;fond=image
;lost=perdu
;nouveaux-segments= nombre de segments restant à ajouter
;menu-opton= afficher le menu des options
;mode-collision=toucher bord fait perdre?
;sinon revenir de l'autre coté

(define-struct point(x y))
(define (point=? p1 p2)
  (and (= (point-x p1) (point-x p2))
        (= (point-y p1) (point-y p2))))



(define serpent0 (list (make-point 3 0)(make-point 2 0)(make-point 1 0)(make-point 0 0)))

(define (m0) (make-monde
           serpent0
           (list (make-bonus serpent0 (list)))
           "right" #f #f 4 #f #f))


;;;;;;;;;;;;;;;;
;Desssin
;;;;;;;;;;;;;;;


(define taille-jeu 25)
(define taille-carreau 25)


(define (make-canevas color) (square (* taille-carreau taille-jeu) "solid" color))

(define scene (make-canevas "black"))

(define canevas (make-canevas "transparent"))
                                      
(define image-bidon (circle 10 "solid" "white"))

(define carreau (square taille-carreau "solid" "white"))

(define (dessiner-carreau point fond)
   (place-image carreau (* (+ 0.5 (point-x point)) taille-carreau) (* (+ 0.5 (point-y point)) taille-carreau)fond))



;(define dessiner-serpent dessiner-carreau
(define (dessiner-segment segment fond)
 (dessiner-carreau segment fond))

(define (dessiner-serpent fond serpent)
  (if (null? serpent)
      fond
     (dessiner-serpent (dessiner-segment (first serpent) fond) (rest serpent))))
  ;(foldl dessiner-segment fond serpent)

(define (dessiner-un-bonus bonus fond)
  (dessiner-carreau bonus fond))

(define (dessiner-les-bonus les-bonus)
  (foldl dessiner-un-bonus canevas les-bonus))  

(define half-height (/(image-height canevas)2))
(define half-width (/(image-width canevas)2))
;half=demis
;height=hauteure
;width=largeure

(define (dessiner-fin perdu?)
(if perdu?
    (place-image (text "Perdu" 70 "orange")  half-width  half-height canevas)
    canevas))

 
  ;(if -cond
     ; -alors
     ; -sinon)

(define (dessiner-menu monde)
 (place-image (text "1. collision avec les bords" 32 (if (monde-bord-collision? monde)
                                                         "green"
                                                         "white")) 0 0
              (place-image (text "2. terrain infini" 32 (if (monde-bord-collision? monde)
                                                         "white"
                                                         "green")) 20 100
                           scene))) 
  

(define (dessiner monde)
  (if (monde-menu-option? monde)
      (dessiner-menu monde)
  ;overlay=superposer
  (overlay
   
   (dessiner-fin (monde-lost? monde))   
   (dessiner-les-bonus (monde-bonus monde))
   (dessiner-serpent canevas (monde-serpent monde))
   scene)))


;;;;;;;;;;;;;;;
; Logique du jeu
;;;;;;;;;;;;;;;


(define(déplacer-tete précédent dir)
  (cond
    [(string=? dir "up")
     (make-point (point-x précédent)(- (point-y précédent) 1))]

    [(string=? dir "down")
     (make-point (point-x précédent)(+ (point-y précédent) 1))]

    [(string=? dir "left")
       (make-point (- (point-x précédent)1)(point-y précédent))]

      [(string=? dir "right")
       (make-point (+ (point-x précédent)1)(point-y précédent))]))

(define (déplacer-corps précédent)
 (cons (first précédent)
   (drop-right (rest précédent) 1)))    
;cons=construire

(define (déplacer-serpent précédent dir)
  (cons (déplacer-tete (first précédent) dir)
        (déplacer-corps précédent)))

(define (grandir-serpent précédent dir)
  (cons (déplacer-tete (first précédent) dir)
        précédent))


(define (sortie? serpent)
  (let ([tete (first serpent)])
    (or (= (point-x tete) taille-jeu)
        (= (point-y tete) taille-jeu)
        (= (point-x tete) -1)
        (= (point-y tete) -1))))

(define (collision? serpent)
  (member (first serpent) (rest serpent) point=?))


(define (monde/serpent-déplacer m)
  (let* ((déplacer? (= (monde-nouveaux-segments m) 0))
         (serpent2 (if déplacer?
                       (déplacer-serpent (monde-serpent m)(monde-dir m))
                       (grandir-serpent (monde-serpent m)(monde-dir m)))))
    (struct-copy monde m
                 [serpent serpent2]
                 [lost? (or (collision? serpent2)
                            (sortie? serpent2))]

                 
                 [nouveaux-segments (if déplacer?
                                        (monde-nouveaux-segments m)
                                        (- (monde-nouveaux-segments m) 1))])))

;(monde-nouveaux-segments m)= 0 que si déplacer est True
;(- (monde-nouveaux-segments m) 1)
;(déplacer-serpent (monde-serpent m)(monde-dir m))


(define (make-bonus serpent les-bonus)
  (let ((bonus (make-point ( random taille-jeu) (random taille-jeu))))
    (if (or (member bonus serpent point=?)
            (member bonus les-bonus point=?))
        (make-bonus serpent les-bonus)
        bonus)))
  ;(append (liste 2 1) (liste 3 4)) =  liste 2 1 3 4)
  ;(random n)= nombre aléatoire entre 0 et n (n non inclus)

(define (monde/bonus-manger m)
  (let* ([tete (first (monde-serpent m))]
        [les-bonus (monde-bonus m)]
        [nouveau-bonus (make-bonus (monde-serpent m) les-bonus)])
   (if (member tete les-bonus point=?)
       (struct-copy monde m
                    [bonus (cons nouveau-bonus (remove tete les-bonus point=?))]
                    [nouveaux-segments (+ (monde-nouveaux-segments m) 2)])
       m)))
    ;cons=faire une nouvelle liste avec un nouveau truc devant     


(define (suivant m)
 (if (or (monde-lost? m) (monde-menu-option? m))
     m
     (monde/bonus-manger (monde/serpent-déplacer m))))
;(make-monde serpent2 (monde-dir monde) (member (first serpent2) (rest serpent2)))
 

;;;;;;;;;;;;;;;
; Boucle du jeu
;;;;;;;;;;;;;;;




(define (clavier m touche)
  ;m=monde
  (cond
    [(member touche (list "up" "down" "left" "right") key=?)
     (struct-copy monde m ( dir touche))]
    [(key=? touche "q")
     (struct-copy monde m (stop #t))]
    [(key=? touche "r")
     (if (monde-lost? m)
         (m0)
         m)]
    [(key=? touche "o")
     (struct-copy monde m [menu-option? #t])]
    [else m]))

;[else=sinon]
; (cond
  ; [(key=? touche "up")
  ;(struct-copy monde m ( dir "up"))] ...)

   
 (define (arret monde)
   (monde-lost? monde))
     ; #t
      ;#f))


;;;;;;;;;;;;;;;;;
; Big bang
;;;;;;;;;;;;;;;;;


(big-bang (m0)
  (stop-when monde-stop)
  (close-on-stop #t)
  (on-key clavier)
  (on-tick suivant 0.5)
  (to-draw dessiner))