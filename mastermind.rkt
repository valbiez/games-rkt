#lang racket

(require rackunit)


(define (correct-positions solution guess)
  (if (null? solution)
      0
      (+
         (if (eq? (car solution) (car guess)) 1 0)
         (correct-positions (cdr solution) (cdr guess)))))


(check-equal? (correct-positions '(#\A #\B #\C #\D) '(#\E #\E #\E #\E)) 0)
(check-equal? (correct-positions '(#\A #\B #\C #\D) '(#\A #\E #\E #\E)) 1)
(check-equal? (correct-positions '(#\A #\B #\C #\D) '(#\E #\B #\E #\E)) 1)
(check-equal? (correct-positions '(#\A #\B #\C #\D) '(#\E #\E #\C #\E)) 1)
(check-equal? (correct-positions '(#\A #\B #\C #\D) '(#\E #\E #\E #\D)) 1)
(check-equal? (correct-positions '(#\A #\B #\C #\D) '(#\A #\E #\C #\D)) 3)
(check-equal? (correct-positions '(#\A #\B #\C #\D) '(#\A #\B #\C #\D)) 4)


(define (member? item lat)
  (if (null? lat)
      #f
      (if (eq? item (car lat)) #t (member? item (cdr lat)))))


(define (remove-member item lat)
 (if (null? lat)
     '()
     (if (eq? item (car lat))
         (cdr lat)
         (cons (car lat) (remove-member item (cdr lat))))))


(define (missplaced-positions solution guess)
  (if (null? guess)
      0
      (+
         (if (member? (car guess) solution) 1 0)
         (missplaced-positions (remove-member (car guess) solution) (cdr guess)))))


(check-equal? (missplaced-positions '(#\A #\B #\C #\D) '(#\E #\E #\E #\E)) 0)
(check-equal? (missplaced-positions '(#\A #\B #\C #\D) '(#\E #\A #\E #\E)) 1)
(check-equal? (missplaced-positions '(#\A #\B #\C #\D) '(#\E #\A #\A #\A)) 1)
(check-equal? (missplaced-positions '(#\A #\B #\C #\D) '(#\D #\A #\B #\C)) 4)


(define (eevaluate solution guess)
  (let (
        [correct (correct-positions solution guess)]
        [missplaced (missplaced-positions solution guess)])
    (list correct (- missplaced correct))))

(check-equal? (eevaluate '(#\A #\B #\C #\D) '(#\E #\E #\E #\E)) '(0 0))
(check-equal? (eevaluate '(#\A #\B #\C #\D) '(#\E #\A #\E #\E)) '(0 1))
(check-equal? (eevaluate '(#\A #\B #\C #\D) '(#\E #\B #\C #\A)) '(2 1))


(let loop ()
    (display "guess ")
    (define guess (read-line (current-input-port) 'any))
    (define result (eevaluate '(#\A #\B #\C #\D) (string->list guess)))
    (if (eq? (car result) 4)
        (display "gagné")
        (
         (display result)
         (loop)
        )))
